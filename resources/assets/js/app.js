
require('./bootstrap');
import VueRouter from 'vue-router';


window.Vue = require('vue');

Vue.use(VueRouter);


let TasksList = Vue.component('tasks-list', require('./components/TasksList.vue'));
let ArchivedTasksList = Vue.component('archived-tasks-list', require('./components/ArchivedTasksList.vue'));


const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes: [
      { path: '/', component: TasksList },
      { path: '/archive', component: ArchivedTasksList },
    ]
  })


const app = new Vue({
    el: '#app',
    router,
}).$mount("#app");
