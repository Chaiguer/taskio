## About the Project

Through this personnel project I aim to put in use some of Laravel and VueJs features such as Json responses, VueJs components, router and ect...
In order to run this project you will need: 

- [Laravel(5.5 or higher)](https://Laravel.com/docs/5.7).
- [NodeJs & npm](https://nodejs.org/en/).
- [Github](https://git-scm.com/downloads).
- [XAMPP](https://www.apachefriends.org/download.html).


## Project installation


In order to run the project correctly, you'll have to follow these steps:

- [Clone the repository](https://bitbucket.org/Chaiguer/taskio) to your local development machine by executing this command:
```git clone https://Chaiguer@bitbucket.org/Chaiguer/taskio.git```.

- In order for our date to be persistent, we will use a **MySql** database. Create a database with any name of your choice (eg: todo-list). In our case the project comes with a custom utility command as follows:
```php artisan make:database --databaseName```
Moreover there is a sql file named as **tasks.sql** which can be imported via a MySql database engine to create a Mysql database todo-list and a table with the names respectively todo-list and tasks.

- Laravel needs to know which database you will use (database name, username, password...), so Laravel added a nameless file with the **env** extension in order to resolve that. Laravel comes with a dummy file named as **.env.example** which we can use to tell Laravel the database information.
- Rename the file (.env.example) to .env.

- Find the row with **DB_DATABASE** and give it as value the name of the database your created.

- Do the same for the following two columns: **DB_USERNAME** and **DB_PASSWORD**(leave empty if there is no password).

- We will need to create a table (tasks) where to store our tasks. Laravel uses a pretty neat concept to create MySql tables know as [migrations](https://Laravel.com/docs/5.5/migrations), the project comes with a migration for the tasks table so what's left is to tell Laravel to create the table in our database, in order to do that use the following command:
```php artisan migrate```

- Since we are using VueJs and some features that require some JavaScript packages, we will need to install the packaged needed by our project using the following command:
```npm install```


## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

# vue-simple-boilerplate

> The simplest possible Vue setup in a single HTML file

> This template is Vue 2.0 compatible. For Vue 1.x use this command: `vue init simple#1.0 my-project`

### Before You Start...

This boilerplate is targeted at beginners who want to start exploring Vue without the distraction of a complicated development environment.

For advanced features such as asset compilation, hot-reload, lint-on-save, unit testing, and CSS extraction, we recommend that more experienced developers use one of the [other templates](https://github.com/vuejs-templates/).

## Usage

This is a project template for [vue-cli](https://github.com/vuejs/vue-cli).

``` bash
$ npm install -g vue-cli      # Install vue-cli if you haven't already
$ vue init simple my-project  # Create a new project based on this template
$ cd my-project               # Navigate into your new project folder

$ npm install -g live-server  # Install live-server if you haven't already
$ live-server                 # Run live-server and open it in your browser
```

### Fork It And Make Your Own

You can [fork this repo](https://help.github.com/articles/fork-a-repo/) to create your own boilerplate, and use it with `vue-cli`:

``` bash
vue init username/repo my-project
```